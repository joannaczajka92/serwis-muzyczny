<?php

@require_once "Session_user.php";

//pobieranie ostatniego id płyty
$sql_id_img = mysqli_query(Session_user::$connection,"SELECT MAX(cover) FROM album");

if ($sql_id_img === FALSE) {
	die(mysqli_error()); 
}

while ($row = mysqli_fetch_array($sql_id_img)) {
	$id_max=$row[0];
}

$id_max_last=$id_max+1; 
//dane płyty z formularza
$title_cd=$_POST['title_cd'];
$band_cd=$_POST['band_cd'];
$select_genre_id=$_POST['select_genre_id'];
$describe_cd=$_POST['describe_cd'];
//dodawanie zespołu
$sql_band="SELECT name FROM band WHERE name='$band_cd'";
$row=mysqli_query(Session_user::$connection, $sql_band);

if (mysqli_num_rows($row)==0) {

	$sql_band_add=mysqli_query(Session_user::$connection,"INSERT INTO band(name) VALUES ('$band_cd')");
}
//dodawanie danych o albumie
$sql_album_add=mysqli_query(Session_user::$connection,"INSERT INTO album(title,describe_cd,rok,rate,cover,status,id_genre) VALUES('$title_cd','$describe_cd','CURRENT_DATE()','0','$id_max_last','1','$select_genre_id')");
//dodawanie album_band
$sql_id_band = mysqli_query(Session_user::$connection,"SELECT id_band FROM band WHERE name='$band_cd'");
$row_id_band = mysqli_fetch_array($sql_id_band);
$sql_id_album= mysqli_query(Session_user::$connection,"SELECT id_album FROM album WHERE title='$title_cd'");
$row_id_album = mysqli_fetch_array($sql_id_album);
$sql_album_band_add=mysqli_query(Session_user::$connection,"INSERT INTO album_band(id_album,id_band) VALUES('$row_id_album[0]','$row_id_band[0]')");

if ($sql_album_add === FALSE) { 
	die(mysqli_error()); 
}
?>