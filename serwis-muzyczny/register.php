<?php 
include "index.php";
@require_once "Session_user.php";

//obsługa formularza dodawania użytkownika
	$valid = true;
	if (strlen($_POST['login_register']) < 3 || strlen($_POST['login_register'])> 10) {
		$valid = false;
		$_SESSION['e_login']= 'Login musi mieć od 3 do 10 znaków! ';
	}

	if (strlen($_POST['password']) < 6 || strlen($_POST['password']) >20) {
		$valid = false;
		$_SESSION['e_pass'] = 'Hasło musi mieć od 6 do 20 znaków!';
	}
	
	if (($_POST['password'] !== $_POST['password_repeat'])) {
		$valid = false;
		$_SESSION['e_pass_rep']= 'Hasła nie są takie same! ';
	}
	
	$pass_hash = password_hash($_POST['password'],PASSWORD_DEFAULT);
			
	if (strlen($_POST['email']) == 0) { //brak pełnej walidacji
		$valid = false;
	}
	
	if ($valid) {
		$query = "INSERT INTO `user`(`login`, `password`,`email`,`id_user_type`)"
		." VALUES(\"".$_POST['login_register'] ."\",\"".$pass_hash ."\",\"".$_POST['email'] ."\",1)";
		
		if (mysqli_query(Session_user::$connection, $query)) {
		
			echo "<script>
					swal({   
					title: \"\",   
					text: \"Konto zostało założone!\",  
					type:'success',
					showConfirmButton:true,
					confirmButtonColor: '#27a387'},
					function(){
					window.location.replace(\"index.php\");
					});
					</script>";
			
		} else {
			die(mysqli_error());	
			echo "Błąd zapytania sql<br/>";
		}

	}else{
		
		echo "<script>
		swal({   
		title: \"\",   
		text: \"Nie można zarejestrować użytkownika! \\n".$_SESSION['e_login']."\\n".$_SESSION['e_pass']."\\n".$_SESSION['e_pass_rep']."\",  
		type:'error',
		showConfirmButton:true,
		confirmButtonColor: '#27a387'},
		function(){
		window.location.replace(\"index.php\");
		});
		</script>";
	}

	unset($_SESSION['e_login']);
	unset($_SESSION['e_pass']);
	unset($_SESSION['e_pass_rep']);
?>
