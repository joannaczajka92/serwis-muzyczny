<div id="modal_register" class="popupContainer" style="display:none;">
	<header class="popupHeader">
		<span class="header_title">Rejestracja</span>
		<span class="modal_close"><i class="fa fa-times"></i></span>
	</header>
		
	<section class="popupBody">
		
		<!-- Username & Password Login form -->
		<div class="user_login">
			<form name="register" action="register.php" method="POST">
				<label>Login (od 3 do 10 znaków)</label>
				<input type="text" name="login_register" required>
				<br/>

				<label>Hasło (od 6 do 20 znaków)</label>
				<input type="password" name="password" required>
				<br/>
				
				<label>Powtórz hasło</label>
				<input type="password" name="password_repeat" required>
				<br/>
				
				<label>Email</label>
				<input type="text" name="email" required>
				<br/>

				<div class="action_btns">
					<div class="one_half last"><input type="submit" class="btn_pop_up btn_red" value="UTWÓRZ KONTO"></div>
				</div>
			</form>
		</div>
	</section>
</div>