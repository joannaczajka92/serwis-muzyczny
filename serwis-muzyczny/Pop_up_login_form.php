<div id="modal_login" class="popupContainer" style="display:none;">
	<header class="popupHeader">
		<span class="header_title">Logowanie</span>
		<span class="modal_close"><i class="fa fa-times"></i></span>
	</header>
		
	<section class="popupBody">
		
		<!-- Username & Password register form -->
		<div class="user_login">
			<form name="login" action="formLogin.php" method="POST">
				<label>Login</label>
				<input type="text" name="login" required>
				<br/>

				<label>Hasło</label>
				<input type="password" name="haslo" required>
				<br/>

				<div class="action_btns">
					<div class="one_half last"><input type="submit" class="btn_pop_up btn_red" value="ZALOGUJ"></div>
				</div>
			</form>
		</div>

	</section>
</div>